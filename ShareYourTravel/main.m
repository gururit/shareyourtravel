//
//  main.m
//  ShareYourTravel
//
//  Created by Guruprasath on 02/04/15.
//  Copyright (c) 2015 Guruprasath. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
